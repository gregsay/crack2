#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"
#include <sys/types.h>
#include <sys/stat.h>

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    
    // Hash the guess using MD5
    char *guessHash = md5(guess, strlen(guess));

    // Compare the two hashes
    // Free any malloc'd memory        
    if (strcmp(guessHash, hash) == 0)
        {
            free (guessHash);
            return 1;
        }
    else
        {
            free (guessHash);
            return 0;
        }        
    
}





// Given a filename, return its length or -1 if error
int file_length(char *filename)
{
        struct stat fileinfo;
        if (stat(filename, &fileinfo) == -1)
            return -1;
        else
            return fileinfo.st_size;
            
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
        // Obtain length of file{ 
        int len = file_length(filename);
        if (len == -1)
        {
            printf("Couldn't get length of file %s\n", filename);
            exit(1);
        }
        
        // Allocate memory for the entire file
        char *file_contents = malloc(len);
        
        // Read the entire file into file_contents
        FILE *fp = fopen(filename, "r");
        if (!fp)
        {
            printf("Couldn't open %s for reading\n", filename);
            exit(1);
        }
        fread(file_contents, 1, len, fp);
        fclose(fp);
        
        // Replace \n  with \0
        // Also keep count as we go.
        int line_count =0;
        for (int i =0; i < len; i++)
        {
            if (file_contents[i] == '\n')
            {
                file_contents[i] = '\0';
                line_count++;
            }
        }
        
        // Allocate array of pointers to each line, with additonal 
        // space for a NULL at the end.
        char **lines = malloc((line_count) * sizeof(char *));// deleted line_count +1
        // Fill in each entry with address of corresponging line
        int c = 0;
        for (int i = 0; i < line_count; i++)
        {
            lines[i] = &file_contents[c];
            
            // Scan forward to find  next line
            while (file_contents[c] != '\0') c++;
            c++;
        }
        
        // Store NULL at the end of the array 
        //lines[line_count] = NULL; Barry deleted
        
        // Return the address of the data structure
        *size = line_count;
        return lines;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary("rockyou100.txt", &dlen);

    // Open the hash file for reading.
    FILE *h;
    h = fopen("hashes.txt", "r");
    
    if (!h)
    {
        printf("Couldn't open hashes.txt\n");
        exit(1);
    }
    
    // For each hash, try every entry in the dictionary. loop hashes file
    char hash [HASH_LEN];
    while(fgets(hash, HASH_LEN, h) != NULL)
    {
        hash[strlen(hash)] = '\0';
        for(int i= 0; i< dlen; i++)
            {
                
                if(tryguess(hash, dict[i]) == 1)
                    {
                        printf("Found match %s\n", dict[i]);
                        break;    
                    }
                    
                  
            }
        
        
//printf("%s ",hash); 

    }
    
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    fclose(h);// close hasshes file
    free(dict[0]);
    free(dict);
}
